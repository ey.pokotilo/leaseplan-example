package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import starter.model.ResponseModel;

import java.net.HttpURLConnection;
import java.util.List;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.is;
import static starter.api.ApiClient.get;

public class SearchStepDefinitions {
    private static final String SEARCH = "search/test/";
    private static final String ROOT_JSON_PATH = ".";
    private static final String DETAIL_ERROR_JSON_PATH = "detail.error";
    private static final String DETAIL_MESSAGE_JSON_PATH = "detail.message";
    private static final String NOT_FOUND_ERROR_MESSAGE = "Not found";


    @When("he calls endpoint to search for {string}")
    public void heCallsEndpointToSearchFor(String string) {
        get(SEARCH + string);
    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedFor(String string) {
        restAssuredThat(response -> response.statusCode(HttpURLConnection.HTTP_OK));
        List<ResponseModel> list = lastResponse().then().extract().jsonPath().getList(ROOT_JSON_PATH, ResponseModel.class);
        list.forEach(x -> assertThat(x.getTitle(), containsStringIgnoringCase(string)));
    }

    @Then("he doesn't not see the results")
    public void he_Does_Not_See_The_Results() {
        restAssuredThat(response -> response.statusCode(HttpURLConnection.HTTP_NOT_FOUND));
        restAssuredThat(response -> response.body(DETAIL_ERROR_JSON_PATH, is(true)));
        restAssuredThat(response -> response.body(DETAIL_MESSAGE_JSON_PATH, is(NOT_FOUND_ERROR_MESSAGE)));
    }
}
