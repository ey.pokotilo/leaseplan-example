package starter.model;

import lombok.Getter;

@Getter
public class ResponseModel {
    private String provider;
    private String title;
    private String url;
    private String brand;
    private Double price;
    private String unit;
    private Boolean isPromo;
    private String promoDetails;
    private String image;
}
