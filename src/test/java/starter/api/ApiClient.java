package starter.api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

public class ApiClient {
    private static final String ENVIRONMENT = "https://waarkoop-server.herokuapp.com/api/v1/";

    public static void get(String url) {
        SerenityRest
                .given(getRequestSpecification())
                .log().all()
                .get(url)
                .then()
                .log().all();
    }

    private static RequestSpecification getRequestSpecification(){
      return new RequestSpecBuilder().setBaseUri(ENVIRONMENT).build();
    }
}
