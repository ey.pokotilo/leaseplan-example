Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario Outline: Search for existing items
    When he calls endpoint to search for "<item>"
    Then he sees the results displayed for "<item>"

    Examples: Existing items
      | item  |
      | apple |
      | mango |

  Scenario: Search for non-existing item
    When he calls endpoint to search for "car"
    Then he doesn't not see the results
