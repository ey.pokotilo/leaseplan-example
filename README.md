## LeasePlan example

### Project structure:
|____src\
| |____test\
| | |____java\
| | | |____starter\
| | | | |____api\
| | | | |____model\
| | | | |____stepdefinitions\
| | |____resources\
| | | |____features
The structure implies that the main source directory is used for the application code (as per the readme file that was inside main).

### Running tests:

Run all the feature files ```mvn verify```

### Serenity report generation:
There are two types of reports that are produced:
- regular Serenity report (index.html) 
- Serenity summary report (serenity-summary.html) - single page report that can be easily shared
both are generated in "/target/site/serenity/" folder

serenity-maven-plugin is configured in a way that both regular Serenity report and Serenity summary report are generated after running ```mvn verify``` command

In case tests are run via IDE, for example, the reports can be generated using the following command: ```mvn serenity:aggregate```

### Writing tests: 
In case we need to add a new scenario, it can be added to an existing feature file or create a new one under "features" folder. Step definitions should be added to an existing "SearchStepDefinitions" class or to a newly created class under "stepdefinitions" package. "ApiClient" class can be extended in case additional logic for sending requests is required, this class contains simple RestAssured specification and a method for sending HTTP GET request, the other ones can be added respectively.
(This description considers API-related scenarios only, just like the ones that are already implemented. UI tests or some other ones would require additional logic.)

### Gitlab CI:
The pipeline is configured in a way that it's triggered on a commit to a master branch. The reports can be found within downloaded artifacts (index.html and serenity-summary.html).

### What's done:
Firstly, the project was cleared, all unnecessary dependencies, files and classes were removed. Since it's agreed to use main source directory for the application code, maven-failsafe-plugin was chosen as the one for running integration Cucumber tests (yet maven-surefire-plugin for unit tests). Although it was mentioned not to update "post_product.feature" file, but I've edited it for several reasons. Scenario did not have a description, and it corrupted the report generation, and there was a typo as well. Also, I had to update the structure of a feature file and divide its scenario because there is one failing test (the search for "mango") and since a failing tests stops the scenario execution it was decided to implement the first two tests in a data-driven way, plus, it's just logical since they are just similar. The logic of the search results verification step is implemented in a way it checks the status code and iterates through all the found entities and checks that their title contains the specific string. (some products do not contain "mango" strings in their titles) As for the search for non-existing product, there is a check of the status code and some fields of the error object.